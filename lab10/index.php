<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8"/>
        <title>Lab 10 </title>
        <link rel="stylesheet" href="style.css" />
    </head>
    
    <body>
        
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbName = "myDB";
        $tableName = "class";
          
        //connect server
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        //create database
        if (!$conn->query("CREATE database IF NOT EXISTS ". $dbName)){
            echo "Error creating database: " . $conn->error;
        }
        
        //connect the database
        if (!$conn->query("USE ". $dbName)){
            echo "Error using database: " . $conn->error;
        }
        
        //create table
        $tabCreate = "create table IF NOT EXISTS " . $tableName . "(
        id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        matricular VARCHAR(15)
        )";
        
        if (!$conn->query($tabCreate)){
            echo "Error creating table: " . $conn->error;
        }
        
        //print datas into a table
        function printTable($db, $table){
            $select = "SELECT * FROM " . $table;
            if ($result = $db->query($select)) {
                echo '<table id="classTable"><tr><th class="first">First Name</th><th class="second">Last Name</th><th class="third">Matricular</th></tr><tr>';
                while ($row = $result->fetch_assoc()) {
                    echo "<tr class='row'><td class='first'>". $row["firstname"] . "</td><td class='second'>" . $row["lastname"] . "</td><td class='third'>" . $row["matricular"] . "</td></tr>";
                }
                echo '</table>';
                $result->free();
            }
        }
            
    ?>
        <header>
            <nav id="nav">
                <ul>
                    <li><a href='#' id="add">Add a student</a></li>
                    <li><a href='#' id="delete">Delete a student</a></li>
                    <li><a href='#' id="set">Set a student</a></li>
                </ul>
            </nav>
        </header>
        
        <main>
            <section id="forms">
                <div id="addForm">
                    <form method="post" action="add.php">
                        <table>
                            <tfoot><tr><td>
                                <input type="submit" name="addStudent" value="Add" id="addStudent"/>
                            </td></tr></tfoot>
                            <tbody>
                                <tr>
                                    <th><label for="fname">First Name</label></th>
                                    <td><input type="text" name= "fname" id="fname"/></td>
                                </tr>
                                <tr>
                                    <th><label for="lname">Last Name</label></th>
                                    <td><input type="text" name= "lname" id="lname"/></td>
                                </tr>
                                <tr>
                                    <th><label for="mat">Matricular</label></th>
                                    <td><input type="text" name= "mat" id="mat"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                <div id="deleteForm">
                    <form method="post" action="delete.php">
                        <table>
                            <tfoot><tr><td>
                                <input type="submit" name="deleteStudent" value="Delete" id="deleteStudent"/>
                            </td></tr></tfoot>
                            <tbody>
                                <tr>
                                    <th><label for="mat">Matricular</label></th>
                                    <td><input type="text" name= "mat" id="mat"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                <div id="setForm">
                    <form method="post" action="set.php">
                        <table>
                            <tfoot><tr><td>
                                <input type="submit" name="setStudent" value="Set" id="setStudent"/>
                            </td></tr></tfoot>
                            <tbody>
                                <tr>
                                    <th><label for="mat">Matricular (requiered)</label></th>
                                    <td><input type="text" name= "mat" id="mat"/></td>
                                </tr>
                                <tr>
                                    <th><label for="fname">New First Name</label></th>
                                    <td><input type="text" name= "fname" id="fname"/></td>
                                </tr>
                                <tr>
                                    <th><label for="lname">New Last Name</label></th>
                                    <td><input type="text" name= "lname" id="lname"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                
            </section>
            
            <section id="tableSection">
                <?php
                    printTable($conn, $tableName);
                ?>
            </section>
        </main>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
        <script src="code.js" type="text/javascript"></script>

    </body>


</html>